package codes.devslab.andoird_viper_pattern.ui.login;

import android.app.Activity;
import android.text.TextUtils;
import codes.devslab.andoird_viper_pattern.data.entity.User;

public class LoginPresenter
    implements LoginContracts.Presenter,
    LoginContracts.InteractorOutput {

  private LoginContracts.View view;
  private LoginContracts.Interactor interactor;
  private LoginContracts.Router router;

  LoginPresenter(LoginContracts.View view) {
    this.view = view;
    interactor = new LoginInteractor(this);
    router = new LoginRouter((Activity) view);
  }

  @Override
  public void onLoginButtonPressed(String username, String password) {
    if (TextUtils.isEmpty(username)) {
      view.showError("Username can't be empty");
      return;
    }

    if (TextUtils.isEmpty(password)) {
      view.showError("Password can't be empty");
      return;
    }

    interactor.login(username, password);
  }

  @Override
  public void onLoginSuccess(User user) {
    router.presentHomeScreen(user);
  }

  @Override
  public void onLoginError(String message) {
    view.showError(message);
  }

  @Override
  public void onDestroy() {
    view = null;
    interactor.unregister();
    interactor = null;
    router.unregister();
    router = null;
  }
}
