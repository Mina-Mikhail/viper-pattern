package codes.devslab.andoird_viper_pattern.ui.login;

import codes.devslab.andoird_viper_pattern.data.entity.User;

public interface LoginContracts {

  interface View {
    void showError(String message);
  }

  interface Presenter {
    void onDestroy();

    void onLoginButtonPressed(String username, String password);
  }

  interface Interactor {
    void unregister();

    void login(String username, String password);
  }

  interface InteractorOutput {
    void onLoginSuccess(User user);

    void onLoginError(String message);
  }

  interface Router {
    void unregister();

    void presentHomeScreen(User user);
  }
}