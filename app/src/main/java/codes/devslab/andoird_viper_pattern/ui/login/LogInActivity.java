package codes.devslab.andoird_viper_pattern.ui.login;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import codes.devslab.andoird_viper_pattern.R;

public class LogInActivity
    extends AppCompatActivity
    implements LoginContracts.View {

  private LoginContracts.Presenter presenter = null;

  @BindView(R.id.etEmail)
  TextInputEditText etUsername;

  @BindView(R.id.etPassword)
  TextInputEditText etPassword;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_log_in);
    ButterKnife.bind(this);

    presenter = new LoginPresenter(this);
  }

  @Override
  public void showError(String message) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
  }

  @OnClick(R.id.btnServerLogin)
  public void onLogInClicked() {
    presenter.onLoginButtonPressed(etUsername.getText().toString().trim(),
        etPassword.getText().toString().trim());
  }

  @Override
  protected void onDestroy() {
    presenter.onDestroy();
    presenter = null;
    super.onDestroy();
  }
}