package codes.devslab.andoird_viper_pattern.ui.login;

import android.app.Activity;
import android.content.Intent;
import codes.devslab.andoird_viper_pattern.data.entity.User;
import codes.devslab.andoird_viper_pattern.ui.home.HomeActivity;

public class LoginRouter
    implements LoginContracts.Router {

  private Activity activity;

  LoginRouter(Activity activity) {
    this.activity = activity;
  }

  @Override
  public void presentHomeScreen(User user) {
    Intent intent = new Intent(activity, HomeActivity.class);
    //intent.putExtra("USER", user);
    activity.startActivity(intent);
    activity.finish();
  }

  @Override
  public void unregister() {
    activity = null;
  }
}
