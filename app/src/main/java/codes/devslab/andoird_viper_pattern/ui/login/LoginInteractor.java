package codes.devslab.andoird_viper_pattern.ui.login;

import codes.devslab.andoird_viper_pattern.data.entity.User;

public class LoginInteractor
    implements LoginContracts.Interactor {

  private LoginContracts.InteractorOutput output;

  LoginInteractor(LoginContracts.InteractorOutput output) {
    this.output = output;
  }

  @Override
  public void login(String username, String password) {
    // Call API for login
    if (username.equals("1") && password.equals("1")) {
      output.onLoginSuccess(new User(username, 707));
    } else {
      output.onLoginError("Please check your credentials");
    }
  }

  @Override
  public void unregister() {
    output = null;
  }
}
